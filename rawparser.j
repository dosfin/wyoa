/* description: Parses end executes mathematical expressions. */

/* lexical grammar */
%lex

%x STRING BLOC

%%
[^\[]*[^\[\s]+			console.log(yytext + ": = CHUNK"); return 'CHUNK';
\s*"["						console.log(yytext + ": ->>BLOC"); this.begin('BLOC');
<BLOC>\s+				/* */
<BLOC>"]"				console.log(yytext + ": <<-BLOC"); this.popState();
<BLOC>["]				console.log(yytext + ": ->>STRING");this.begin('STRING');
<BLOC>"->"[A-Za-z0-9_]+	console.log(yytext + ": = TARGET"); yytext =yytext.substr(2); return 'TARGET'
<BLOC>[;]				console.log(yytext + ": = MISC"); return yytext;
<STRING>"\\".			console.log(yytext + ": appended to string"); this.more();
<STRING>["]				console.log(yytext + ": <<-STRING = STRING"); this.popState(); yytext = yytext.slice(0, -1); return 'STRING';
<STRING>[^\\"]+			console.log(yytext + ": appended to string"); this.more();
<*>.					console.log("[1;31m" + yytext + ": ??????[0m");
<<EOF>>					return 'EOF';

/lex

/* operator associations and precedence */

%start expressions

%% /* language grammar */

expressions : l EOF { console.log(require('util').inspect($1,false, null)); return $1;}
    		;

l	: l CHUNK 	{{ $1.chunks.push($2); }}
 	| l jumps 	{{ $$.push($2); }}
	| 		{{ $$ = { "sections": [ {"chunks": [], "actions": [], "jumps": [] }; }}
	;

e	: CHUNK {{ $$ = { "chunk": $1 }; }}
	| b  
    ;

b	: jumps {{ $$ = { "jumps": $1 }; }}
	;

jumps	: jumps ';' jump {{ $1.push($3); }}
		| jump {{ $$ = [ $1 ]; }}
		;

jump	: string target {{ $$ = { "phrase": $1, "target": $2 }; }}
	| target {{ $$ = { "target": $1 }; }}
	;

string	: STRING {{ $$ = yytext; }}
		;

target  : TARGET {{ $$ = yytext; }}
		;
